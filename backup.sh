#!/usr/bin/env bash
cd mispBU
history > history.txt
yum list installed > yum.packages.installed.txt
pip freeze > root.pip3.packages.installed.txt
pip list >> root.pip3.packages.installed.txt
tar -czvf misp_backup.tar.gz /var/www/MISP/*
tar -czvf misp_yum_pip_history.tar.gz /root/mispBU/*
scp misp_yum_pip_history.tar.gz unkn0wn@10.8.65.82:/Users/unkn0wn/MISPbu
rm * -f